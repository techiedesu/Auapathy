namespace Auapathy.Models.Database
{
    /// <summary>
    /// Account entity
    /// </summary>
    internal sealed class Account : DatabaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Secret { get; set; }
        public ushort Picture { get; set; }
    }
}
