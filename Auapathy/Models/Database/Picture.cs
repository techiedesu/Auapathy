namespace Auapathy.Models.Database
{
    public class Picture
    {
        public ushort Id { get; set; }
        
        /// <summary>
        /// Public picture code
        /// </summary>
        public string ExtGuid { get; set; }
        
        /// <summary>
        /// PNG 
        /// </summary>
        public byte[] Body { get; set; }
    }
}