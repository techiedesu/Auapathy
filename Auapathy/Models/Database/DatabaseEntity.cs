namespace Auapathy.Models.Database
{
    public abstract class DatabaseEntity
    {
        public int Id { get; set; }
    }
}
