using Microsoft.Data.Sqlite;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Auapathy.Models.Database
{
  /// <summary>
  /// Base Database Repository
  /// </summary>
  /// <typeparam name="T">Database Entity</typeparam>
  internal sealed class BaseRepository<T> where T : DatabaseEntity
  {
    private readonly string _connectionString;

    public BaseRepository(IConfiguration config)
    {
      _connectionString = config.GetSection("Database:connectionString").Value;
    }

    /// <summary>
    /// Get database entity
    /// </summary>
    /// <param name="id">Id</param>
    /// <param name="allowEmpty">Allow null instead exception</param>
    /// <returns>Database entity</returns>
    /// <exception cref="DatabaseException">Empty or database error</exception>
    public T Get(int id, bool allowEmpty)
    {
      string table = typeof(T).Name.ToLower();
      string getCommandQuery = $"SELECT * FROM {table} WHERE Id = {id} LIMIT 1";

      using (var conn = new SqliteConnection(_connectionString))
      {
        using (var comm = new SqliteCommand(getCommandQuery, conn))
        {
          comm.Parameters.AddWithValue("@Table", table);
          comm.Parameters.AddWithValue("@Id", id.ToString());

          using (var reader = comm.ExecuteReader())
          {
            if (!allowEmpty)
            {
              if (!reader.Read()) throw new DatabaseException("Cannot read database.");
              if (!reader.HasRows) throw new DatabaseException("Empty set.");
            }
            else
            {
              if (!reader.Read() || !reader.HasRows) return null;
            }

            var entity = JsonConvert.DeserializeObject<T>((string)reader["Data"]);
            entity.Id = (int)reader["Id"];

            return entity;
          }
        }
      }
    }

    /// <summary>
    /// Add entity to database
    /// </summary>
    /// <param name="entity">Database Entity</param>
    /// <returns></returns>
    /// <exception cref="DatabaseException">Database error</exception>
    public void Add(T entity)
    {
      string table = typeof(T).Name.ToLower();
      string entityData = JsonConvert.SerializeObject(entity);

      if (!IsTableExists(table)) CreateTable(table);

      string insertCommandQuery = $"INSERT INTO {table}(Data) VALUES (@Data)";

      using (var conn = new SqliteConnection(_connectionString))
      {
        using (var comm = new SqliteCommand(insertCommandQuery, conn))
        {
          comm.Parameters.AddWithValue("@Data", entityData);

          conn.Open();

          try
          {
            comm.ExecuteNonQuery();
          }
          catch (SqliteException ex)
          {
            throw new DatabaseException(ex.Message);
          }

          conn.Close();
        }
      }
    }

    /// <summary>
    /// Update database entity
    /// </summary>
    /// <param name="entity">Database entity</param>
    /// <returns></returns>
    /// <exception cref="DatabaseException">Database error or cannot update database entity</exception>
    public T Update(T entity)
    {
      if (!IsTableExists(typeof(T).Name.ToLower())) throw new DatabaseException();

      string table = typeof(T).Name.ToLower();
      string entityId = entity.Id.ToString();
      string entityData = JsonConvert.SerializeObject(entity);

      string insertCommandQuery = $"UPDATE {table} SET Data = @Data WHERE Id = @Id";

      using (var conn = new SqliteConnection(_connectionString))
      {
        using (var comm = new SqliteCommand(insertCommandQuery, conn))
        {
          comm.Parameters.AddWithValue("@Data", entityData);
          comm.Parameters.AddWithValue("@Id", entityId);

          comm.ExecuteNonQuery();
        }
      }

      return null;
    }

    private bool IsTableExists(string table)
    {
      string getCommandQuery = "SELECT name FROM sqlite_master WHERE type='table' AND name=@Table LIMIT 1;";

      using (var conn = new SqliteConnection(_connectionString))
      {
        using (var comm = new SqliteCommand(getCommandQuery, conn))
        {
          comm.Parameters.AddWithValue("@Table", table);

          conn.Open();

          using (var reader = comm.ExecuteReader())
          {
            var rows = reader.HasRows;
            conn.Close();
            return rows;
          }
        }
      }
    }

    private void CreateTable(string table)
    {
      string sql = $"CREATE TABLE {table} " +
                   "(Id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE, " +
                   "Data TEXT NOT NULL UNIQUE CHECK (Data <> ''));";

      using (var conn = new SqliteConnection(_connectionString))
      {
        using (var comm = new SqliteCommand(sql, conn))
        {
          comm.Parameters.AddWithValue("@Table", table);

          conn.Open();
          comm.ExecuteNonQuery();
          conn.Close();
        }
      }
    }
  }
}
