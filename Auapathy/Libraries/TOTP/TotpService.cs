using System;

namespace Auapathy.Libraries.TOTP
{
  public class TotpService
  {
    protected byte[] Secret;
    protected ushort GenerateStep;
    protected ushort CodeSize;
    protected long TimeOffset;

    public TotpService(byte[] secret, ushort time = 30, ushort size = 6, long timeOffset = 0)
    {
      Secret = secret;
      GenerateStep = time;
      CodeSize = size;
      TimeOffset = timeOffset;
    }

    public int GetCode()
    {
      throw new NotImplementedException("WIP");
    }
  }
}
