using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace Auapathy.Views
{
  public class CodesListView : UserControl
  {
    public CodesListView()
    {
      this.InitializeComponent();
    }

    private void InitializeComponent()
    {
      AvaloniaXamlLoader.Load(this);
    }
  }
}
